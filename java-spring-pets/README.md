Учебная задача.

Оригинальный репозиторий: [GitHub - spring-projects/spring-petclinic: A sample Spring-based application](https://github.com/spring-projects/spring-petclinic)

Подзадачи:

- обернуть приложение в docker

- добавить проксирующий nginx

- переселить приложение в k8s, описать в helm

- в k8s добавить elk, prometheus, grafana, opentelemetry

- настроить сбор логов и метрик

- настроить трейсинг для приложения

- настроить ci + деплой в k8s

- 
